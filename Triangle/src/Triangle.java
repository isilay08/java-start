import java.util.Scanner;

public class Triangle {

    private String type;
    public int sideLength;//kenar uzunluğu
    public int differentEdge;//farklı kenar
    public int otherDifferentEdge;//Diğer farklı kenar
    public int enviroment;//çevre
    public int u;
    public int domain;//alan

    public Triangle(String type) {

        if (type.equals("eskenar")) {
            System.out.println("Lütfen eskenar ücgenin kenar uzunlugunu giriniz");
            Scanner scan = new Scanner(System.in);
            sideLength = scan.nextInt();
            enviroment=calculatePerimeter(sideLength, sideLength, sideLength);
            domain=calculateDomain(sideLength, sideLength, sideLength);
            System.out.println("Ücgenin cevresi:"+enviroment);
            System.out.println("Üçgenin alanı = "+domain);


        }
        if (type.equals("ikizkenar")) {
            System.out.println("Lütfen ikizkenar ücgenin eşit olan ve farklı olan kenar uzunluğunu giriniz");
            Scanner scan = new Scanner(System.in);
            System.out.println("Eşit olan uzunluk");
            sideLength = scan.nextInt();
            System.out.println("Farklı olan uzunluk");
            differentEdge=scan.nextInt();
            enviroment=calculatePerimeter(sideLength,sideLength,differentEdge);
            domain=calculateDomain(sideLength,sideLength,differentEdge);
            System.out.println(type+" "+ "Üçgenin cevresi: "+ enviroment);
            System.out.println(type+" "+ "Üçgenin alanı: "+ domain);


        }
        if (type.equals("cesitkenar")) {
            System.out.println("Lütfen cesitkenar ücgenin  farklı olan kenar uzunluklarını giriniz");
            Scanner scan = new Scanner(System.in);
            sideLength = scan.nextInt();
            differentEdge=scan.nextInt();
            otherDifferentEdge=scan.nextInt();
            enviroment=calculatePerimeter(sideLength,differentEdge,otherDifferentEdge);
            domain=calculateDomain(sideLength,differentEdge,otherDifferentEdge);
            System.out.println(type+" "+ "Üçgenin cevresi: "+ enviroment);
            System.out.println(type+" "+ "Üçgenin alanı: "+ domain);

        }

    }


    //Çevre hesaplama
    private int calculatePerimeter(int a,int b,int c){
        enviroment=(int)(a+b+c);
        return enviroment;

    }

    //Alan hesaplama
    private int calculateDomain(int a,int b,int c){
        u=(a+b+c)/2;
        domain = (int) Math.sqrt(u * (u - a) * (u - b) * (u - c));
        return domain;
    }



}
