# TRIANGLE CLASSES

![ucgen resmi bulunamadi](../trianglesmall.png)

✨✨

## Üçgen sınıfının özellikleri
>  ***Üçgenin tipi(string)(eşkenar,ikizkenar veya çeşitkenar)***

>  *Üçgenin tipi nesne oluşturma aşamasında belirleniyor.Ve bu 3 seçenekten başka bir tipi olmayacak.*  

>  *Kenarlarının uzunluğunu kullanıcıdan alır.*  

>  *Tipi girilen üçgenin alanını hesaplar.*  

>  *Tipi girilen üçgenin çevresini hesaplayan fonksiyon içerir.*  